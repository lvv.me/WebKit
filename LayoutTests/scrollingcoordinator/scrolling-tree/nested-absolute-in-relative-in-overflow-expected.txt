abs
abs

(Frame scrolling node
  (scrollable area size 800 600)
  (contents size 800 600)
  (parent relative scrollable rect at (0,0) size 800x600)
  (scrollable area parameters 
    (horizontal scroll elasticity 2)
    (vertical scroll elasticity 2)
    (horizontal scrollbar mode 0)
    (vertical scrollbar mode 0))
  (layout viewport at (0,0) size 800x600)
  (min layout viewport origin (0,0))
  (max layout viewport origin (0,0))
  (behavior for fixed 0)
  (children 7
    (Overflow scrolling node
      (scrollable area size 285 285)
      (contents size 285 556)
      (parent relative scrollable rect at (30,22) size 285x285)
      (scrollable area parameters 
        (horizontal scroll elasticity 0)
        (vertical scroll elasticity 0)
        (horizontal scrollbar mode 0)
        (vertical scrollbar mode 0)
        (has enabled vertical scrollbar 1))
    )
    (Positioned node
      (layout constraints 
        (layer-position-at-last-layout (10,10))
        (positioning-behavior moves))
      (related overflow nodes 1)
    )
    (Positioned node
      (layout constraints 
        (layer-position-at-last-layout (0,30))
        (positioning-behavior moves))
      (related overflow nodes 1)
    )
    (Positioned node
      (layout constraints 
        (layer-position-at-last-layout (13,43))
        (positioning-behavior moves))
      (related overflow nodes 1)
    )
    (Positioned node
      (layout constraints 
        (layer-position-at-last-layout (53,83))
        (positioning-behavior moves))
      (related overflow nodes 1)
    )
    (Positioned node
      (layout constraints 
        (layer-position-at-last-layout (65,113))
        (positioning-behavior moves))
      (related overflow nodes 1)
    )
    (Positioned node
      (layout constraints 
        (layer-position-at-last-layout (80,110))
        (positioning-behavior moves))
      (related overflow nodes 1)
    )
  )
)

